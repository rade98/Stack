 
 <?php

    public function generate_all(){
        if(isset($_POST['generate_settings']) && is_array($_POST['generate_settings'])){

            $generate_settings = $_POST['generate_settings'];

            $for_generate = array();
            $single_generate = false;

            foreach($generate_settings as $invoice_db_id=>$invoice_settings){
                $invoice_data = array("invoice_db_id"=>$invoice_db_id,"invoice_id"=>$invoice_settings['invoice_id']);
                if(isset($invoice_settings['single_generate']) && $invoice_settings['single_generate']=="yes"){
                    $single_generate = true;
                    $for_generate[] = $invoice_data;
                    break;
                } else {
                    $for_generate[] = $invoice_data;
                }
            }

            if($single_generate==true){
                echo $this->session->flashdata('my_id');
                $generate_response = $this->generateInvoice($for_generate[0]['invoice_db_id'], $for_generate[0]['invoice_id']);
        
            } else {
                foreach ($for_generate as $invoice) {
                    $generate_response = $this->generateInvoice($invoice['invoice_db_id'], $invoice['invoice_id']);
                }

            }
        }

redirect('/invoiceRequests');

    }

